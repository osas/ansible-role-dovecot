# Ansible role for Dovecot installation

## Introduction

[Dovecot](http://dovecot.org/) is a IMAP/POP3 server and MDA.

This role installs and configure the server.

## Authentication

Once configured (see below depending on the selected method), you can
test your setup using:

    doveadm auth test <user>

### PAM (default)

If you want to use PAM authentication, set `auth` to `pam`.

You then may want to adapt the global PAM configuration or
`/etc/pam.d/dovecot` more specifically, but this is outside of
this role's scope; please at your distribution documentation.
For example, look at `authconfig` on RedHat systems and
`pam-auth-update` on Debian.

### YAML User Files

To use this method, set `auth` to `yaml-dict`.

In this method user data (auth and Dovecot specific override
parameters) are stored in simple YAML files, one per user.
These files are stored in `/etc/dovecot/users/`, named `<user>.yml`
(<user> being the Dovecot username), and have the following minimal
structure:

    ---
    uid: 10000
    gid: 10000
    password: "{SHA512-CRYPT}$6$Y/lNk8OLZtMhcnn8$28.CkPlbr7J.4QzsadEqzlgNol1VSfDphmOKgqLVj/bo5Hize7cKIzYOsBDeDpYWHbUWlzIT0mJtOv8L4kXxU1"

The exact list of possible fields map the ones in Dovecot, see the
following documentations:

- http://wiki.dovecot.org/PasswordDatabase
- http://wiki.dovecot.org/UserDatabase

The password field can be generated using the `doveadm` command. First choose a password scheme (the hash method) using:

    doveadm pw -l

Then you can generate a password using this scheme with:

    doveadm pw -s <scheme>

(you'll be prompted for the clear text version of the password twice,
and the result must be copied entirely into the `password` field in the
file).

These files are read by a dict server, on-demand, when Dovecot needs
to access the password or user database. More info on this setup can
be read here: http://wiki.dovecot.org/AuthDatabase/Dict

Installation of these files is ouside the scope of this role. Your
playbook or even specific roles may add their own users by simply
adding files in the directory.

## Variables

- **auth**: "pam" or "yaml-dict" (see the *Authentication* chapter)
            (defaults to "pam")
- **with_imap**: activate the IMAP server if true (defaults to true)
- **with_pop3**: activate the POP3 server if true (defaults to false)
- **with_tls**: activate TLS for all services; access on non-TLS ports
                is still possible from the local machine
                (defaults to true)
- **cert_file**: path for the public part of the server certificate
                 (defaults to `/etc/pki/dovecot/certs/dovecot.crt`)
- **key_file**: path for the private part of the server certificate
                (defaults to `/etc/pki/dovecot/private/dovecot.key`)
- **ca_file**: path for the CA certificate
               (defaults to `/etc/pki/dovecot/certs/dovecot_ca.crt`)

